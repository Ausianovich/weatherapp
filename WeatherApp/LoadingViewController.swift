//
//  ViewController.swift
//  WeatherApp
//
//  Created by Ausianovich Kanstantsin on 28.01.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import UIKit
import Foundation

final class LoadingViewController: UIViewController {
    
    //MARK: - Property
    private let locationManager = LocationManager()
    private let dataLoader = DataLoader()
    
    //MARK: - IBOutlets
    @IBOutlet var loadingLabel: UILabel!
    
    //MARK: - Main functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.alertCompletion = {
            let alert = UIAlertController(title: "Location Services disabled", message: "Please enable Location Services in Settings", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if UserDefaults.isFirstLaunch() {
            UserDefaults.standard.set(true, forKey: "hasBeenLaunchedBeforeFlag")
            let storyboard = UIStoryboard(name: String(describing: TutorialScreenViewController.self), bundle: nil)
            if let tutorialViewController = storyboard.instantiateViewController(withIdentifier: String(describing: TutorialScreenViewController.self)) as? TutorialScreenViewController {
                tutorialViewController.view.alpha = 0.6
                tutorialViewController.view.backgroundColor = .black
                tutorialViewController.modalPresentationStyle = .overCurrentContext
                self.present(tutorialViewController, animated: true)
            }
        }
        
        self.locationManager.locationCompletion = {[weak self] (coordinate, city) in
            self?.dataLoader.getTemp(from: coordinate) {[weak self] (temperature) in
                let message = "In \(city) now \(temperature) degree"
                self?.setWeatherScreen(with: message)
            }
        }
    }
    
    //MARK: - Flow functions
    private func setWeatherScreen(with message: String) {
        let storyboard = UIStoryboard(name: String(describing: WeatherScreenViewController.self), bundle: nil)
        guard let weatherViewController = storyboard.instantiateViewController(identifier: String(describing: WeatherScreenViewController.self)) as? WeatherScreenViewController else {return}
        let weatherViewModel = WeatherScreenViewControllerViewModel(temperatureMessage: message)
        weatherViewController.configure(with: weatherViewModel)
        weatherViewController.modalPresentationStyle = .overFullScreen
        self.present(weatherViewController, animated: true)
    }
}
