//
//  LocationManager.swift
//  WeatherApp
//
//  Created by Ausianovich Kanstantsin on 03.02.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import Foundation
import CoreLocation

final class LocationManager: NSObject {
    
    //MARK: - Property
    private var locationManager = CLLocationManager()
    var alertCompletion: (()->Void)?
    var locationCompletion: ((CLLocationCoordinate2D, String)->Void)?
    
    //MARK: - Main functions
    override init() {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
    }
}

//MARK: - CLLocationManagerDelegate
extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            self.locationManager.requestWhenInUseAuthorization()
        case .denied, .restricted:
            guard let alert = self.alertCompletion else {return}
            alert()
            return
        case .authorizedAlways, .authorizedWhenInUse:
            break
        @unknown default:
            fatalError()
        }
        self.locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let currentLocation = locations.last {
            self.getPlace(for: currentLocation) { (placemark) in
                guard let placemark = placemark else {return}
                guard let city = placemark.locality else {return}

                guard let locationCompletion = self.locationCompletion else {return}
                locationCompletion(currentLocation.coordinate, city)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}

extension LocationManager {
    func getPlace(for location: CLLocation, completion: @escaping (CLPlacemark?) -> Void) {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
            guard error == nil else {
                completion(nil)
                return
            }
            
            guard let placemark = placemarks?.first else {
                completion(nil)
                return
            }
            completion(placemark)
        }
    }
}
