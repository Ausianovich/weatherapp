//
//  WeatherScreenViewController.swift
//  WeatherApp
//
//  Created by Ausianovich Kanstantsin on 04.02.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import UIKit

final class WeatherScreenViewController: UIViewController {
    
    @IBOutlet private var screenNameLabel: UILabel!
    @IBOutlet private var temperatureLabel: UILabel!
    
    private var temperatureMessage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.temperatureLabel.text = self.temperatureMessage
    }
    
    func configure(with viewModel: WeatherScreenViewControllerViewModel) {
        self.temperatureMessage = viewModel.temperatureMessage
    }
}
