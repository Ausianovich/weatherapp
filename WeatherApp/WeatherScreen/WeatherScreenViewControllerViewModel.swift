//
//  WeatherScreenViewControllerViewModel.swift
//  WeatherApp
//
//  Created by Ausianovich Kanstantsin on 04.02.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import Foundation

struct WeatherScreenViewControllerViewModel {
    var temperatureMessage: String
}
