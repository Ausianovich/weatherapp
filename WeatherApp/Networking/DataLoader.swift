//
//  DataLoader.swift
//  WeatherApp
//
//  Created by Ausianovich Kanstantsin on 04.02.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import Foundation
import Alamofire
import CoreLocation

struct DataLoader {
    
    private let url = "https://api.weather.yandex.ru/v1/forecast?"
    private let apiKey = ["X-Yandex-API-Key": "dd136092-7cbd-4f04-b3c2-fe035522bf30"]
    private let decoder = JSONDecoder()
    
    func getTemp (from coordinate: CLLocationCoordinate2D, completion: @escaping (String)->Void ){
        let parameters = ["lat": coordinate.latitude,"lon": coordinate.longitude]
        
        request(self.url, method: .get, parameters: parameters, headers: self.apiKey).responseData { (response) in
            switch response.result {
            case .success(let data):
                do {
                    let weather = try self.decoder.decode(Weather.self, from: data)
                    guard let fact = weather.fact,
                        let temp = fact.temp else {return}
                    completion(temp.description)
                } catch {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
