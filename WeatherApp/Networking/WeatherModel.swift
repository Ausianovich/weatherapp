//
//  WeatherModel.swift
//  WeatherApp
//
//  Created by Ausianovich Kanstantsin on 03.02.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import Foundation
// MARK: - Weather
struct Weather: Codable {
    let now: Int?
    let nowDt: String?
    let info: Info?
    let fact: Fact?
    let forecasts: [Forecast]?

    enum CodingKeys: String, CodingKey {
        case now
        case nowDt = "now_dt"
        case info, fact, forecasts
    }
}

// MARK: - Fact
struct Fact: Codable {
    let temp, feelsLike: Int?
    let icon, condition: String?
    let windSpeed, windGust: Double?
    let windDir: String?
    let pressureMm, pressurePa, humidity: Int?
    let daytime: String?
    let polar: Bool?
    let season: String?
    let precType: Int?
    let precStrength: Double?
    let cloudness, obsTime: Int?
    let fallbackTemp, fallbackPrec: Bool?

    enum CodingKeys: String, CodingKey {
        case temp
        case feelsLike = "feels_like"
        case icon, condition
        case windSpeed = "wind_speed"
        case windGust = "wind_gust"
        case windDir = "wind_dir"
        case pressureMm = "pressure_mm"
        case pressurePa = "pressure_pa"
        case humidity, daytime, polar, season
        case precType = "prec_type"
        case precStrength = "prec_strength"
        case cloudness
        case obsTime = "obs_time"
        case fallbackTemp = "_fallback_temp"
        case fallbackPrec = "_fallback_prec"
    }
}

// MARK: - Forecast
struct Forecast: Codable {
    let date: String?
    let dateTs, week: Int?
    let sunrise, sunset: String?
    let moonCode: Int?
    let moonText: String?
    let parts: Parts?
    let hours: [Hour]?

    enum CodingKeys: String, CodingKey {
        case date
        case dateTs = "date_ts"
        case week, sunrise, sunset
        case moonCode = "moon_code"
        case moonText = "moon_text"
        case parts, hours
    }
}

// MARK: - Hour
struct Hour: Codable {
    let hour: String?
    let hourTs, temp, feelsLike: Int?
    let icon, condition: String?
    let windSpeed, windGust: Double?
    let windDir: String?
    let pressureMm, pressurePa, humidity: Int?
    let precMm: Double?
    let precPeriod, precType: Int?
    let precStrength, cloudness: Double?
    let fallbackTemp, fallbackPrec: Bool?
    let tempMin, tempMax, tempAvg: Int?
    let daytime: String?
    let polar: Bool?

    enum CodingKeys: String, CodingKey {
        case hour
        case hourTs = "hour_ts"
        case temp
        case feelsLike = "feels_like"
        case icon, condition
        case windSpeed = "wind_speed"
        case windGust = "wind_gust"
        case windDir = "wind_dir"
        case pressureMm = "pressure_mm"
        case pressurePa = "pressure_pa"
        case humidity
        case precMm = "prec_mm"
        case precPeriod = "prec_period"
        case precType = "prec_type"
        case precStrength = "prec_strength"
        case cloudness
        case fallbackTemp = "_fallback_temp"
        case fallbackPrec = "_fallback_prec"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case tempAvg = "temp_avg"
        case daytime, polar
    }
}

// MARK: - Parts
struct Parts: Codable {
    let night, evening: Hour?
    let dayShort: Fact?
    let nightShort: Hour?
    let morning, day: Day?

    enum CodingKeys: String, CodingKey {
        case night, evening
        case dayShort = "day_short"
        case nightShort = "night_short"
        case morning, day
    }
}

// MARK: - Day
struct Day: Codable {
}

// MARK: - Info
struct Info: Codable {
    let lat, lon: Double?
    let tzinfo: Tzinfo?
    let defPressureMm, defPressurePa: Int?
    let url: String?

    enum CodingKeys: String, CodingKey {
        case lat, lon, tzinfo
        case defPressureMm = "def_pressure_mm"
        case defPressurePa = "def_pressure_pa"
        case url
    }
}

// MARK: - Tzinfo
struct Tzinfo: Codable {
    let offset: Int?
    let name, abbr: String?
    let dst: Bool?
}
