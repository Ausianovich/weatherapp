//
//  UserDefaults+isFirstLaunch.swift
//  WeatherApp
//
//  Created by Ausianovich Kanstantsin on 02.02.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import Foundation
import UIKit

extension UserDefaults {
    static func isFirstLaunch() -> Bool {
        let hasBeenLaunchedBeforeFlag = "hasBeenLaunchedBeforeFlag"
        let isFirstLaunch = !UserDefaults.standard.bool(forKey: hasBeenLaunchedBeforeFlag)
 
        return isFirstLaunch
    }
}
