//
//  TutorialPageViewController.swift
//  WeatherApp
//
//  Created by Ausianovich Kanstantsin on 30.01.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import UIKit

final class TutorialPageViewController: UIPageViewController {
    
    //MARK: - Property
    private var viewModel: TutorialPageViewControllerViewModel?
    private var currentIndex = 0
    var currentPage: ((Int)->Void)?
    var changeButton: (()->Void)?

    //MARK: - Main functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        self.configure(with: TutorialPageViewControllerViewModel())
        
        for view in self.view.subviews {
            if let scrollView = view as? UIScrollView {
                scrollView.delegate = self
            }
        }
    }
    
    func configure(with viewModel: TutorialPageViewControllerViewModel) {
        self.viewModel = viewModel
        
        if let startingController = self.contentViewController(at: self.currentIndex) {
            self.setViewControllers([startingController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    //MARK: - Flow fucntions
    func getCurrentIndex() -> Int {
        guard let index = (self.viewControllers?.first as? TutorialContentViewController)?.index else {return 0}
        return min(max(index,0), (self.viewModel?.getViewCount() ?? 0) - 1 )
    }
    
    func getNumberOfPages() -> Int {
        self.viewModel?.getViewCount() ?? 0
    }
    
    func nextViewController () {
        guard let nextController = self.contentViewController(at: self.getCurrentIndex() + 1) else {return}
        self.setViewControllers([nextController], direction: .forward, animated: true, completion: { [weak self] _ in
            self?.currentIndex += 1
            guard let currentPage = self?.currentPage else { return }
            currentPage(self?.currentIndex ?? 0)
            guard let changeButton = self?.changeButton else {return}
            changeButton()
        })
    }
}

//MARK: - UIScrollViewDelegate
extension TutorialPageViewController: UIScrollViewDelegate {
        
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let viewCount = self.viewModel?.getViewCount() else {return}
        if self.currentIndex == 0 && scrollView.contentOffset.x < scrollView.bounds.size.width {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        } else if self.currentIndex == viewCount - 1 && scrollView.contentOffset.x >= scrollView.bounds.size.width {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        }
    }

    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        guard let viewCount = self.viewModel?.getViewCount() else {return}
        if self.currentIndex == 0 && scrollView.contentOffset.x < scrollView.bounds.size.width {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        } else if currentIndex == viewCount - 1 && scrollView.contentOffset.x >= scrollView.bounds.size.width {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        }
    }
}

//MARK: - UIPageViewControllerDelegate
extension TutorialPageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard completed else {return}
        self.currentIndex = self.getCurrentIndex()
        guard let currentPage = self.currentPage else { return }
        currentPage(self.currentIndex)
        guard let changeButton = self.changeButton else {return}
        changeButton()
    }
}

//MARK: - UIPageViewControllerDataSource
extension TutorialPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard var index = (viewController as? TutorialContentViewController)?.index else {return nil}
        index -= 1
        return self.contentViewController(at: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard var index = (viewController as? TutorialContentViewController)?.index else {return nil}
        index += 1
        return self.contentViewController(at: index)
    }
    
    private func contentViewController(at index: Int) -> TutorialContentViewController? {
        guard let viewModel = self.viewModel else {return nil}
        
        if index < 0 || index >= viewModel.getViewCount() {
            return nil
        }
        let storyboard = UIStoryboard.init(name: String(describing: TutorialContentViewController.self), bundle: nil)
        guard let viewController = storyboard.instantiateViewController(identifier: String(describing: TutorialContentViewController.self)) as? TutorialContentViewController else {return nil}
        guard let model = self.viewModel?.getModel(at: index) else {return nil}
        let contentViewModel = TutorialContentViewControllerViewModel(model: model)
        viewController.configure(with: contentViewModel)
        viewController.index = index
        return viewController
    }
}
