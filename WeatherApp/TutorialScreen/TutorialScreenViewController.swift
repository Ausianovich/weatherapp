//
//  TutorialScreenViewController.swift
//  WeatherApp
//
//  Created by Ausianovich Kanstantsin on 29.01.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import UIKit
import YandexMobileMetrica

final class TutorialScreenViewController: UIViewController {

    //MARK: - Property
    private var tutorialPageViewController: TutorialPageViewController?
    private var currentPage: Int = 0
    private var numberOfPages: Int = 0
    
    //MARK: - IBOutlets
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var nextButton: UIButton!
    
    //MARK: - Main functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let tutorialPageViewController = self.children.first as? TutorialPageViewController else {
            fatalError()
        }
        self.tutorialPageViewController = tutorialPageViewController
        self.configurePageControl()
        self.configureNextButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        _ = LocationManager()
    }

    //MARK: - IBActions
    @IBAction private func dismissVC(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func nextPageButton(_ sender: UIButton) {
        if self.currentPage == self.numberOfPages - 1 {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.tutorialPageViewController?.nextViewController()
        }
        
        let params: [String: Any] = ["Click 1": "Page 1", "Click 2":"Page 2"]

        YMMYandexMetrica.reportEvent("Next button is pressed", parameters: params) { (error) in
            print(error.localizedDescription)
        }
    }
    
    //MARK: - Flow functions
    private func configurePageControl() {
        guard let pageViewController = self.tutorialPageViewController else {return}
        self.pageControl.numberOfPages = pageViewController.getNumberOfPages()
        self.numberOfPages = pageViewController.getNumberOfPages()
        
        self.tutorialPageViewController?.currentPage = { page in
            self.currentPage = page
            self.pageControl.currentPage = page
            self.pageControl.updateCurrentPageDisplay()
        }
    }
    
    private func configureNextButton() {
        self.nextButton.layer.cornerRadius = self.nextButton.frame.size.height / 2
        self.tutorialPageViewController?.changeButton = {
            if self.currentPage == self.numberOfPages - 1 {
                self.nextButton.setTitle("Done", for: .normal)
            } else {
                self.nextButton.setTitle("Next", for: .normal)
            }
        }
    }
}
