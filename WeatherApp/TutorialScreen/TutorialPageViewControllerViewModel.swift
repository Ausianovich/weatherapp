//
//  TutorialPageViewControllerViewModel.swift
//  WeatherApp
//
//  Created by Ausianovich Kanstantsin on 30.01.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import Foundation

struct TutorialPageViewControllerViewModel {
    private let pageContent: [PageContentModel] = [
                                PageContentModel(headerText: "Header page 1", messageText: "Message page 1"),
                                PageContentModel(headerText: "Header page 2", messageText: "Message page 2"),
                                PageContentModel(headerText: "Header page 3", messageText: "Message page 3"),
                                PageContentModel(headerText: "Header page 4", messageText: "Message page 4")
                                                    ]
    
    func getViewCount() -> Int {
        return self.pageContent.count
    }
    
    func getModel(at index: Int) -> PageContentModel {
        return self.pageContent[index]
    }
}
