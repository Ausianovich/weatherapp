//
//  TutorialContentViewControllerViewModel.swift
//  WeatherApp
//
//  Created by Ausianovich Kanstantsin on 30.01.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import Foundation

struct TutorialContentViewControllerViewModel {
    
    var headerText: String
    var messageText: String
    
    init(model: PageContentModel) {
        self.headerText = model.headerText
        self.messageText = model.messageText
    }
}
