//
//  TutorialContentViewController.swift
//  WeatherApp
//
//  Created by Ausianovich Kanstantsin on 30.01.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import UIKit

final class TutorialContentViewController: UIViewController {

    //MARK: - Property
    private var headerText: String = ""
    private var messageText: String = ""
    var index = 0
    
    //MARK: - IBOutlets
    @IBOutlet private var headerLabel: UILabel!
    @IBOutlet private var messageLabel: UILabel!
    
    //MARK: - Main functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.headerLabel.text = self.headerText
        self.messageLabel.text = self.messageText
    }

    func configure(with viewModel: TutorialContentViewControllerViewModel) {
        self.headerText = viewModel.headerText
        self.messageText = viewModel.messageText
    }
}
