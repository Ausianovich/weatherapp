//
//  PageContentModel.swift
//  WeatherApp
//
//  Created by Ausianovich Kanstantsin on 30.01.2020.
//  Copyright © 2020 Constantine inc. All rights reserved.
//

import Foundation

struct PageContentModel {
    var headerText: String
    var messageText: String
}
